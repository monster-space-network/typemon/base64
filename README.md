# Base64 - [![version](https://img.shields.io/npm/v/@typemon/base64.svg)](https://www.npmjs.com/package/@typemon/base64) [![license](https://img.shields.io/npm/l/@typemon/base64.svg)](https://gitlab.com/monster-space-network/typemon/base64/blob/master/LICENSE) ![typescript-version](https://img.shields.io/npm/dependency-version/@typemon/base64/dev/typescript.svg)



## Installation
```
$ npm install @typemon/base64
```

### Browser polyfill
- https://github.com/feross/buffer
```
$ npm install buffer
```
```typescript
import { Buffer } from 'buffer/';
```
```typescript
Object.defineProperty(window, 'Buffer', Buffer);
```



## Usage
```typescript
import { Base64 } from '@typemon/base64';
```

### Encode
```typescript
Base64.encode('Monster Space Network - Typemon - Base64');
Base64.encodeAsync('Monster Space Network - Typemon - Base64');
```
```
TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA==
```

### Encode URL
```typescript
Base64.encodeURL('Monster Space Network - Typemon - Base64');
Base64.encodeURLAsync('Monster Space Network - Typemon - Base64');
```
```
TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA
```

### Decode
```typescript
Base64.decode('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA==');
Base64.decode('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA');
Base64.decodeAsync('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA==');
Base64.decodeAsync('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA');
```
```
<Buffer ...>
```
