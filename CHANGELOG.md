# [2.0.0](https://gitlab.com/monster-space-network/typemon/base64/compare/1.2.0...2.0.0) (2020-01-12)


### Bug Fixes

* **encode:** 버퍼 이중 처리 문제 수정 ([1dbf930](https://gitlab.com/monster-space-network/typemon/base64/commit/1dbf930e8c7e8e2f78b16806bc24009feec0b375))


### Features

* **decode:** 버퍼 문자열 처리 삭제 ([89132a6](https://gitlab.com/monster-space-network/typemon/base64/commit/89132a6bd91d1e834e436f1dc1035e244b1af559))
* null 삭제, 비동기 처리 코드 리팩터링 ([d6da724](https://gitlab.com/monster-space-network/typemon/base64/commit/d6da724905cf6bb7c3425b2047c72447ea9bef4a))
* 의존성 업데이트 ([501561f](https://gitlab.com/monster-space-network/typemon/base64/commit/501561f7432a285ac2089383f26094b16d32d8fe))



# [1.2.0](https://gitlab.com/monster-space-network/typemon/base64/compare/1.1.0...1.2.0) (2019-10-18)


### Features

* 의존성 업데이트 ([3bda5ec](https://gitlab.com/monster-space-network/typemon/base64/commit/3bda5ec89b035a1a040f8d83debba076eb6f24b0))



# [1.1.0](https://gitlab.com/monster-space-network/typemon/base64/compare/1.0.0...1.1.0) (2019-09-18)


### Features

* **package:** 의존성 업데이트, 테스트 의존성 변경 ([afc1d3f](https://gitlab.com/monster-space-network/typemon/base64/commit/afc1d3f))
* null, null-url 추가 ([f5025b3](https://gitlab.com/monster-space-network/typemon/base64/commit/f5025b3))
* 비동기 기능 추가 ([dffade7](https://gitlab.com/monster-space-network/typemon/base64/commit/dffade7))



