import { Base64 } from './base64';
//
//
//
const testObject: object = { foo: 'bar' };
const stringifiedObject: string = JSON.stringify(testObject);
const encodedObject: string = Buffer.from(stringifiedObject).toString('base64');

test('encode', () => {
    expect(Base64.encode('Monster Space Network - Typemon - Base64')).toBe('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA==');
    expect(Base64.encode(Buffer.from('Monster Space Network - Typemon - Base64'))).toBe('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA==');
    expect(Base64.encode(testObject)).toBe(encodedObject);
    expect(Base64.encodeURL('Monster Space Network - Typemon - Base64')).toBe('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA');
    expect(Base64.encodeURL(Buffer.from('Monster Space Network - Typemon - Base64'))).toBe('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA');
    expect(Base64.encodeURL(testObject)).toBe(encodedObject.replace(/\+/g, '-').replace(/\//g, '_').replace(/\=/g, ''));
});
test('encode-async', () => {
    expect(Base64.encodeAsync('Monster Space Network - Typemon - Base64')).resolves.toBe('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA==');
    expect(Base64.encodeAsync(Buffer.from('Monster Space Network - Typemon - Base64'))).resolves.toBe('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA==');
    expect(Base64.encodeAsync(testObject)).resolves.toBe(encodedObject);
    expect(Base64.encodeURLAsync('Monster Space Network - Typemon - Base64')).resolves.toBe('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA');
    expect(Base64.encodeURLAsync(Buffer.from('Monster Space Network - Typemon - Base64'))).resolves.toBe('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA');
    expect(Base64.encodeURLAsync(testObject)).resolves.toBe(encodedObject.replace(/\+/g, '-').replace(/\//g, '_').replace(/\=/g, ''));
});

test('decode', () => {
    expect(Base64.decode('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA==').toString()).toBe('Monster Space Network - Typemon - Base64');
    expect(Base64.decode('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA').toString()).toBe('Monster Space Network - Typemon - Base64');
    expect(Base64.decode(encodedObject).toString()).toBe(stringifiedObject);
});
test('decode-async', () => {
    expect(Base64.decodeAsync('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA==').then((buffer: Buffer): string => buffer.toString())).resolves.toBe('Monster Space Network - Typemon - Base64');
    expect(Base64.decodeAsync('TW9uc3RlciBTcGFjZSBOZXR3b3JrIC0gVHlwZW1vbiAtIEJhc2U2NA').then((buffer: Buffer): string => buffer.toString())).resolves.toBe('Monster Space Network - Typemon - Base64');
    expect(Base64.decodeAsync(encodedObject).then((buffer: Buffer): string => buffer.toString())).resolves.toBe(stringifiedObject);
});
