import { Check } from '@typemon/check';
//
//
//
export namespace Base64 {
    export function encode(value: unknown): string {
        if (Buffer.isBuffer(value)) {
            return value.toString('base64');
        }

        const data: string = Check.isString(value)
            ? value
            : JSON.stringify(value);
        const encoded: string = Buffer
            .from(data)
            .toString('base64');

        return encoded;
    }
    export function encodeURL(value: unknown): string {
        const encoded: string = encode(value);
        const escaped: string = encoded
            .replace(/\+/g, '-')
            .replace(/\//g, '_')
            .replace(/\=/g, '');

        return escaped;
    }

    export function decode(value: string): Buffer {
        return Buffer.from(value, 'base64');
    }

    function runAsync<Value>(callback: () => Value): Promise<Value> {
        return new Promise((resolve: (value: Value) => void): void => {
            setImmediate((): void => resolve(callback()));
        });
    }

    export function encodeAsync(value: unknown): Promise<string> {
        return runAsync((): string => encode(value));
    }
    export function encodeURLAsync(value: unknown): Promise<string> {
        return runAsync((): string => encodeURL(value));
    }

    export function decodeAsync(value: string): Promise<Buffer> {
        return runAsync((): Buffer => decode(value));
    }
}
